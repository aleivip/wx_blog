const util = require('./util.js');

function connect(user, func) {
	wx.connectSocket({
		url: util.wssPath + '/chat/v1/message?friendphone=' + user.friendInfo.account + '&userphone=' + user.userInfo.account + '&app_sid=' + user.userInfo.app_sid,
		header: { 'content-type': 'application/json' },
		success: function (res) {
			console.log(res)
		},

		fail: function (res) {
			console.log(res);
		}
	});

	wx.onSocketOpen(function (res) {
		
		//接受服务器消息
		wx.onSocketMessage(func);//func回调可以拿到服务器返回的数据
	});

	wx.onSocketError(function (res) {
		wx.showToast({
			title: res.errMsg,
			icon: "none",
			duration: 1000
		});
	});
}

//发送消息
function send(msg) {
	wx.sendSocketMessage({
		data: msg
	});
}

module.exports = {
	connect: connect,
	send: send
}