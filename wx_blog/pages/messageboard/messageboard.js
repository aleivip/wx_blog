const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;

Page({
  data: {
    author_rhesis: '无穷的伟大，也是从"一"开始的。',
    isAuthor: false,
    tip: "",
    loading: false,
    isShow: false,
    increase: false,
    aniStyle: true,
    messageboard: [],
    message: '',
    emoji: ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35'],
		emojiChar: '😓-😡-😳-😁-😠-😪-😄-😄-😔-😒-😞-😖-😏-😥-😷-😚-😂-😉-😵-☺-😌-😣-😥-😭-😱-😰-😨-😍-😃-😊-😝-😋-🍓-☀-☁-☔'
  },

  onLoad: function() {
    var that = this;
		var chatInfo = wx.getStorageSync("chatInfo");
		that.setData({chatInfo: chatInfo});
		this.getMessageBoard();
  },

	getMessageBoard: function() {
		var that = this;
		wx.request({
			url: util.basePath + '/article/v1/getMessageBoard',
			method: "post",
			data: {
				chatInfo: that.data.chatInfo,
				page: pageNo
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					that.setData({
						messageboard: that.data.messageboard.concat(res.data.payload)
					});
				} else {
					that.setData({tip: '没有更多数据了', isShow: true})
				}
			}
		});
	},   

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading();
		this.data.messageboard = [];
    pageNo = 1;
		this.getMessageBoard();

    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1000);
  },

  onReachBottom: function() {
    var that = this;
    ++pageNo;
		this.getMessageBoard();
  },          

  onShareAppMessage: function(res) {
    return {
      title: '空城丶Blog',
      path: '/pages/index/index'
    }
  },

  //监听input值的改变
  messageInput(res) {
    this.setData({
      message: res.detail.value
    });
  },

  cleanInput() {
    //button会自动清空，所以不能再次清空而是应该给他设置目前的input值
    this.setData({
      message: this.data.message
    });
  },

  increase() {
    this.setData({
      increase: !this.data.increase,
      aniStyle: true
    });
  },
          
  emojiChoose: function(e) {
    var index = e.target.dataset.id
    var emojiArr = this.data.emojiChar.split('-');
    this.setData({
      message: this.data.message + emojiArr[index]
    });
  },

  //点击空白隐藏message下选框
  outbtn() {
    this.setData({
      increase: false,
      aniStyle: true
    });
  },
	
	send: function() {
		this.setData({
			increase: false
		});

		if(!this.data.message) {
			$Toast({
				content: '不能留言空消息!',
				type: 'error'
			});
		} else {
			//chatInfo 中 friendInfo 是作者信息，userInfo 是留言者信息也就是登陆者信息
			var that = this;
			var chatInfo = that.data.chatInfo;
			chatInfo.message = that.data.message;

			wx.request({
				url: util.basePath + '/article/v1/saveMessageBoard',
				method: "post",
				data: {
					chatInfo: chatInfo
				},
				header: {
					'content-type': 'application/json'
				},
				success(res) {
					if (res.data.status == 200) {
						$Toast({
							content: res.data.payload,
							type: 'success'
						});

						that.setData({message: '', messageboard: []});
						that.onLoad();
					} else {
						$Toast({
							content: res.data.err,
							type: 'error'
						});
					}
				}
			});
		}
	}
});