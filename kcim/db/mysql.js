/**
 * Created by pkd on 14-3-18.
 */
// require("../util/bootloader");
var CONFIG = require("../config.js");
var mysql = require('mysql');

var mysqlDb = function () {
    this.pool = null;
    this._init();
};

mysqlDb.prototype._init = function () {
    this.pool = mysql.createPool(CONFIG.TEST_DB);
    console.log("mysqlDb is initialized.\n");
};

mysqlDb.prototype.getConnection = function (cb) {
    var pool = this.getPool();
    if (pool) {
        pool.getConnection(function (err, connection) {
            cb(err, connection);
        });
    } else {
        cb("db factory is not init.");
    }
};

mysqlDb.prototype.poolQuery = function (sql, data, cbIfNoConnection, cbQuery) {
    var pool = this.getPool();
    if (pool) {
        pool.getConnection(function (err, connection) {
            if (err) {
                if (cbIfNoConnection) cbIfNoConnection(err, connection);
                if (connection) connection.release();
                return;
            }
            connection.query(sql, data, function (error, result) {
                if (!cbQuery) {
                    connection.release();
                    return;
                }
                if (!cbQuery(error, result, connection)) {
                    connection.release();
                }
            });
        })
    } else {
        if (cbIfNoConnection) cbIfNoConnection("db factory is not init.");
    }
};
//获取连接池对象
mysqlDb.prototype.getPool = function () {
    if (!this.pool) {
        this._init();
    }
    return this.pool;
};

//通用sql执行方法，回调函数参数和原始api类似
mysqlDb.prototype.query = function (sql, values, cb) {
    var self = this;
    var pool = self.getPool();
    pool.query(sql, values, cb);
};

mysqlDb.prototype.executeSql = function (sql, data, type, cb) {
    mysqlDb.prototype.poolQuery(sql, data, function () {
        cb('no collection');
    }, function (err, result) {
        if (err) {
            cb(err);
            return;
        }
        cb(null, result);
    });
}

//查询列表类方法
mysqlDb.prototype.poolQueryList = function (sql, sqlCount, value, cb) {
    if (sql == undefined) return cb('poolQueryList Err:参数有误');
    if (sqlCount == undefined) return cb('poolQueryList Err:参数有误');
    if (value == undefined) return cb('poolQueryList Err:参数有误');
    mysqlDb.prototype.poolQuery(sqlCount, value,
        function () {
            cb('no collection');
        },
        function (err, result, connection) {
            if (err) {
                console.log('total Err: ' + err);
                cb(err);
                return;
            }
            var res = {
                total: 0,
                data: []
            };
            if (!result || result.length <= 0) {
                cb(null, res);
                return;
            }
            res.total = result[0].total;
            connection.query(sql, value, function (err1, result1) {
                connection.release();
                if (err1) {
                    cb(err1);
                    return;
                }
                res.data = result1;
                cb(null, res);
            });
            return true;
        }
    );
};

module.exports = new mysqlDb();
