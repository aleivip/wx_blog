#### 介绍
小程序版博客，目前已上线功能：博客系统，好友系统和会话系统。

#### 软件架构
## 博客系统
用户在首页可以分页浏览所有文章，也可以按照分类去浏览对应分类的文章。分类目前就是固定的几种分类。数据量目前全来源于一个支持爬取数据的技术社区，所以数据来源完全合法。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/152648_89e77e7b_4769407.png "屏幕截图.png")

可以在顶部的搜索框进行关键字搜索，找对应关键字的文章数据。不过这里有一个小缺点，由于我忙着先将基本构架做出来所以我对搜索结果还未来得及做分页，所以比较热门的关键字去搜索可以响应时间会比较慢。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171500_6fcd1da1_4769407.png "屏幕截图.png")

点击文章进入文章详情界面，文章详情界面采用towxml插件去加载markdown格式的html文本渲染，所以渲染效果比较美观。目前只支持文本和图片也就是图文格式，后期会开放音频等文件。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171537_ceff0046_4769407.png "屏幕截图.png")

在文章右下角可以对文章进行点赞和评论。设计评论功能的时候一直在和朋友探讨，究竟评论功能放在文章底部合适还是放在一个新界面合适呢？由于考虑到有很多时候技术文章都会是长文，可能用户还没看完就不想看了，一些优质评论根本没看到，所以采用将评论功能设计为一个新的界面，点击右下角评论图标即可进入评论界面。然后点击底部写评论按钮即可弹出评论框。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/152956_5e7b3f68_4769407.png "屏幕截图.png")

在文章详情界面点击用户头像可以进入作者主页，如果非好友关系可以添加作者为好友。作者主页可以按分类查看作者的全部原创文章。 实际上我也是写这篇文章的时候才发现主图为长图布局会出现拉伸，这个小问题预计下个版本修复。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/153140_a4008ffb_4769407.png "屏幕截图.png")

在写作界面可以进入个人主页和进行文章写作。个人主页可以按照分类查看自己的所有发表文章。点击文章进入文章详情界面，长按文章进入可以选择修改或删除文章。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/153421_d9cae4da_4769407.png "屏幕截图.png")

在导航栏写作界面可以进入个人主页和进行文章写作。个人主页可以按照分类查看自己的所有发表文章。点击文章进入文章详情界面，长按文章进入可以选择修改或删除文章。在写作界面可以进行文章的写作，文章写作界面采用一段文字一张配图的方式，文字输入域不允许为空，第一张图片必须上传作为文章主图，第二段文本开始可以不上传图片。 这里讲讲为什么采用这种多段文本的方式呢？其实我们都知道pc端我们文章写作一般都是markdown方式，双屏写作，可以边写作边预览，但是移动端很明显没法做到，因为手机屏幕太小了不可能再分成双屏写作，那我们需要实现图文格式要怎么办呢？所以我想到了一个折中的方法，我将写作界面设计为多段图文输入，刚进入默认只有一个图文，第一段图文图片必须上传，图片会作为文章主图，当我们第一段图文写作完成，可以选择右下角的+,添加下一段图文，不过从第二段开始，可以不上传图片只输入纯文本。效果其实就如同下图一样的效果，然后点击发表的时候就会通过特定的算法去手动将文本和图片转化成markdown格式的html文本去保存，也是为了能够使用towxml插件去加载文章内容，使界面更为美观。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/153621_6b4aa6c4_4769407.png "屏幕截图.png")

文章写作完成可以点击右上角预览按钮进行文章效果预览，预览效果和发布后完全一致。 实际上在预览的时候就是将文章内容通过同一个算法转化成markdown格式的html文本，然后使用towxml去加载html文本渲染出页面效果。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/153717_bf9b1e26_4769407.png "屏幕截图.png")

在这里遇到了一个小难题，就是文章修改的时候，由于我们将用户输入的文本和上传的图片手动去封装成html文本，所以修改文章时需要将文章内容去反向进行解析，分别解析出每一段文本和每一张图片，然后分别放入图片数组和文本数组中，这个反向解析一共40行代码，但是花费了一早上时间才成功反向解析，可以给你们看看代码：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171710_d3e1abc3_4769407.png "屏幕截图.png")

## 好友系统 && 会话系统

用户添加好友主要有两个途径：第一个途径就是刚才说过的在文章详情界面点击作者头像进入作者主页添加作者好友，或者在好友界面顶部搜索域输入用户手机号搜索好友并发送好友申请，好友申请可以在导航栏我的界面中的消息界面中查看并同意好友申请。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/153945_db2f221f_4769407.png "屏幕截图.png")

已成功添加好友会在好友列表中显示。当好友有新信息会在列表中显示未读信息条数。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/154119_e9199fb0_4769407.png "屏幕截图.png")

点击好友可以进入实时会话，会话是实时会话，有新消息送达会自动渲染

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/154228_f6564112_4769407.png "屏幕截图.png")

点击用户发送的图片可以查看大图。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171819_1a6a4ec5_4769407.png "屏幕截图.png")

在好友列表长按某个好友可以设置备注，设置特别关心，删除好友等操作。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/154315_dc8cec7b_4769407.png "屏幕截图.png")

用户发表的文章如果有其他用户点赞或者评论，以及好友申请都可以在导航栏我的中的消息中查看，有新消息会显示红点显示未读消息个数。 

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171902_157f3513_4769407.png "屏幕截图.png")

消息中分为三个板块：好友申请、评论、点赞。好友申请通过或者拒绝则申请消息就会变成已读，评论和点赞目前无法对单条消息设置已读操作，只能点击底部全部已读对全部消息设置已读。点击全部已读则红点就会消失。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171936_e408df6e_4769407.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/171947_4a7e0d06_4769407.png "屏幕截图.png")

阅读文章无需授权登录就可以自由阅读，如果要使用好友系统和实时会话系统以及文章写作功能就需要授权登录绑定手机号才可进行。目前整个项目前后端已开源于码云，欢迎来一个star。源码地址：

> https://gitee.com/mqzuimeng_admin/wx_blog.git

欢迎体验小程序，如果有修改建议欢迎加我微信聊聊。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/172049_72815990_4769407.png "屏幕截图.png")

欢迎关注公众号：程序猿周先森。公众号有个人微信联系方式和技术群二维码。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1106/172134_8550f92b_4769407.png "屏幕截图.png")


#### 安装教程

1.  项目前端为wx_blog，直接微信开发者工具打开即可运行
2.  后端架构采用Nginx + NodeJS + Mysql，项目导入然后更改数据库配置即可使用

