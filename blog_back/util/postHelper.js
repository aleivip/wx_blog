/**
 * Created by apple on 2017/7/6.
 */

var postHelp = module.exports;
var request = require('request');

postHelp.baseRequest = function (url, param, callback) {
    url += '?';
    for (var key in param) {
        if (!param[key]) {
            continue;
        }
        url += key + '=' + param[key] + '&';
    }
    url = url_encode(url.substr(0, url.length - 1));

    var options = {
        url: url,
        method: 'POST'
    };


    request(options, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            callback('服务器异常');
            return;
        } else {
            if (body == 'success') {
                callback(null, body);
                return;
            }
            if (body && typeof body != 'string') {
                callback(null, body);
                return;
            }
            try {
                // if (body) {
                //     body = JSON.parse(body);
                // }
                callback(null, body);
            } catch (e) {
                callback(body);
            }
        }
    });
}
postHelp.baseRequestByGet = function (url, param, callback) {
    url += '?';
    for (var key in param) {
        if (!param[key]) {
            continue;
        }
        url += key + '=' + param[key] + '&';
    }
    url = url_encode(url.substr(0, url.length - 1));

    var options = {
        url: url,
        method: 'get'
    };

    request(options, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            callback('服务器异常');
            return;
        } else {
            if (body == 'success') {
                callback(null, body);
                return;
            }
            if (body && typeof body != 'string') {
                callback(null, body);
                return;
            }
            try {
                body = JSON.parse(body);
                callback(null, body);
            } catch (e) {
                callback(e);
            }
        }
    });
}

postHelp.baseRequestBase = function (url, param, callback) {
    var options = {
        url: url,
        method: 'POST',
        headers: {
            Host: 'aip.baidubce.com',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            image: param.image,
            id_card_side: param.id_card_side,
            request_id: param.request_id,
            result_type: param.result_type
        },
        timeout: 10000
    };

    if (!param.image) {
        delete options.form.image;
    }
    if(!param.id_card_side){
        delete options.form.id_card_side;
    }
    if(!param.request_id){
        delete options.form.request_id;
    }
    if (!param.result_type) {
        delete options.form.result_type;
    }
    
    request(options, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            callback('服务器异常');
            return;
        } else {
            if (body == 'success') {
                callback(null, body);
                return;
            }
            if (body && typeof body != 'string') {
                callback(null, body);
                return;
            }
            try {
                if (body) {
                    body = JSON.parse(body);
                }
                callback(null, body);
            } catch (e) {
                callback(e);
            }
        }
    });
}

postHelp.baseRequestbdai = function (url, param, callback) {
    var options = {
        url: url,
        method: 'POST',
        headers: {
            Host: 'aip.baidubce.com',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            imgUrl: param.imgUrl,
            imgType: param.imgType
        },
        timeout: 10000
    };

    param.text ? options.form.text = param.text : '';

    request(options, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            return callback('服务器异常');
        } 
        
        if (body == 'success') {
            return callback(null, body);
        }
        if (body && typeof body != 'string') {
            return callback(null, body);
        }
        try {
            callback(null, JSON.parse(body));
        } catch (e) {
            callback(e);
        }
    });
}

postHelp.baseRequestByJson = function (url, param, callback) {
    var option = {
        url: url,
        method: "POST",
        json: true,
        headers: {
            "content-type": "application/json",
        },
        body: param
    };

    request(option, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            callback('服务器异常，请稍后重试1');
            console.log(error);
            return;
        } else {
            if (body == 'success') {
                callback(null, body);
                return;
            }

            if (body && typeof body != 'string') {
                callback(null, body);
                return;
            }

            try {
                body = JSON.parse(body);
                callback(null, body);
            } catch (e) {
                callback('服务器异常，请稍后重试2');
            }
        }
    });
}

function url_encode(url) {
    url = encodeURIComponent(url);
    url = url.replace(/\%3A/g, ":");
    url = url.replace(/\%2F/g, "/");
    url = url.replace(/\%3F/g, "?");
    url = url.replace(/\%3D/g, "=");
    url = url.replace(/\%26/g, "&");

    return url;
}
