var express = require('express');
var jwt = require('jsonwebtoken');
var CONFIG = require('./../config');
var ArticleDao = require('./../dao/article');
require('./../util/common');
require('./../util/bootloader');
var router = express.Router();

router.post('/getArticleList', function (req, res) {
    var par = paramAll(req);

    if (!par.page) {
        return res.json(new ERR('页码不为空!', 400));
    }
    if (!par.size) {
        par.size = 10;
    }

    ArticleDao.getArticleList(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

//获取留言
router.post('/getMessageBoard', function (req, res) {
    var par = paramAll(req);

    if (!par.page || !par.chatInfo) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getMessageBoard(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } 
        
        return res.json(new PKG(data));
    });
});

//留言
router.post('/saveMessageBoard', function (req, res) {
    var par = paramAll(req);

    if (!par.chatInfo) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.saveMessageBoard(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } 
        
        return res.json(new PKG(data));
    });
});

//收藏
router.post('/saveCollectMessage', function (req, res) {
    var par = paramAll(req);

    ArticleDao.saveCollectMessage(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } 
        
        return res.json(new PKG(data));
    });
});

//删除聊天记录
router.post('/deleteOnlineMessage', function (req, res) {
    var par = paramAll(req);

    ArticleDao.deleteOnlineMessage(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } 
        
        return res.json(new PKG(data));
    });
});

router.post('/keywordSearchArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.page || !par.size || !par.cateUrl) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.keywordSearchArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/categories', function (req, res) {
    var par = paramAll(req);

    ArticleDao.categories(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/searchArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.page) {
        par.page = 1;
    }

    ArticleDao.searchArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/getUserArticleList', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getUserArticleList(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/articleDetail/:id', function (req, res) {
    var id = req.params.id;

    ArticleDao.articleDetail(id, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            return res.json(new PKG(data));
        }
    });
});

router.post('/saveArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.title || !par.gist || !par.content || !par.account || !par.app_sid || !par.labels) {
        return res.json(new ERR('参数不全!', 400));
    }

    par.labels = par.labels.toString();

    ArticleDao.saveArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/updateArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.title || !par.gist || !par.content || !par.labels || !par.id || !par.main_pic || !par.article_content || !par.article_img) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.updateArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/changeContentStatus', function (req, res) {
    var par = paramAll(req);

    if (!par.id) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.changeContentStatus(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/deleteArticle/:id', function (req, res) {
    var id = req.params.id;

    ArticleDao.deleteArticle(id, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/addArticleStar', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid || !par.article_id || !par.openid) {
        return res.json(new ERR('账号不为空!', 400));
    }

    ArticleDao.addArticleStar(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/checkUserStar', function (req, res) {
    var par = paramAll(req);

    if (!par.account) {
        return res.json(new ERR('账号不为空!', 400));
    }
    if (!par.app_sid) {
        return res.json(new ERR('平台标识不为空!', 400));
    }
    if (!par.article_id) {
        return res.json(new ERR('文章id不为空!', 400));
    }

    ArticleDao.checkUserStar(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/checkUserRead', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid || !par.article_id) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.checkUserRead(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});


router.post('/addArticleComment', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid || !par.article_id || !par.comment || !par.username || !par.avatar || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.addArticleComment(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/wx_news', function (req, res) {
    ArticleDao.wx_news({}, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/rand_article', function (req, res) {
    ArticleDao.rand_article({}, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/articleComments/:id', function (req, res) {
    var id = req.params.id;

    ArticleDao.articleComments(id, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        } else {
            if (data.status == 200) {
                return res.json(new PKG(data));
            } else {
                return res.json(new ERR(data, 420));
            }
        }
    });
});

router.post('/saveContent', function (req, res) {
    var par = paramAll(req);

    if (!par.title || !par.gist || !par.content || !par.labels || !par.author || !par.openid || !par.main_pic || !par.article_content || !par.article_img) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.saveContent(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/userDesignArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.article_id || !par.myphone || !par.app_sid || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.userDesignArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/authorDesignArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.authorDesignArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/userDesignArticleLabels', function (req, res) {
    var par = paramAll(req);

    if (!par.article_id || !par.myphone || !par.app_sid || !par.openid || !par.cateUrl) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.userDesignArticleLabels(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/authorDesignArticleLabels', function (req, res) {
    var par = paramAll(req);

    if (!par.app_sid || !par.openid || !par.cateUrl) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.authorDesignArticleLabels(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/addUserApply', function (req, res) {
    var par = paramAll(req);

    if (!par.myphone || !par.app_sid || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.addUserApply(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

router.post('/checkStatus', function (req, res) {
    var par = paramAll(req);

    if (!par.v || par.v != '1.0.8') {
        return res.json(new PKG('成功'));
    }

    return res.json(new ERR('暂未开放', 400));
});

//获取用户好友申请消息
router.post('/getUserMessage', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getUserMessage(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//获取评论消息
router.post('/getCommentMessage', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getCommentMessage(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//获取点赞消息
router.post('/getStarMessage', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getStarMessage(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//拒绝好友申请
router.post('/refuseApply', function (req, res) {
    var par = paramAll(req);

    if (!par.id) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.refuseApply(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//同意好友申请
router.post('/consentApply', function (req, res) {
    var par = paramAll(req);

    if (!par.id || !par.myphone || !par.friendphone) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.consentApply(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//获取好友列表
router.post('/getFriendList', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }
    if (!par.page) {
        par.page = 1;
    }

    ArticleDao.getFriendList(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//删除好友
router.post('/deleteFriend', function (req, res) {
    var par = paramAll(req);

    if (!par.id) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.deleteFriend(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//获取用户发表文章和分类信息
router.post('/getUserArticleAndCategories', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getUserArticleAndCategories(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//30秒自动保存草稿功能
router.post('/saveDraftContent', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid || !par.title || !par.author || !par.labels || !par.avatar) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.saveDraftContent(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//判断草稿箱是否有文章
router.post('/checkDraftContent', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.checkDraftContent(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//删除草稿文章
router.post('/delDraftContent', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.delDraftContent(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//获取草稿文章内容
router.post('/getDraftContentDetail', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getDraftContentDetail(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//按照分类获取用户发表文章
router.post('/getLabelsArticle', function (req, res) {
    var par = paramAll(req);

    if (!par.openid || !par.app_sid || !par.cateUrl) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getLabelsArticle(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//查询好友信息
router.post('/getFriendInfo', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.getFriendInfo(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//判断是否为好友关系
router.post('/checkIsFriend', function (req, res) {
    var par = paramAll(req);

    if (!par.myphone || !par.friendphone) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.checkIsFriend(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//设置备注
router.post('/renameFriend', function (req, res) {
    var par = paramAll(req);

    if (!par.id || !par.rename) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.renameFriend(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//设置关心
router.post('/setFocusFriend', function (req, res) {
    var par = paramAll(req);

    if (!par.id) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.setFocusFriend(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//查看未读消息个数
router.post('/unreadCount', function (req, res) {
    var par = paramAll(req);

    if (!par.account || !par.app_sid || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.unreadCount(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//将评论全部设为已读
router.post('/changeCommentType', function (req, res) {
    var par = paramAll(req);

    if (!par.app_sid || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.changeCommentType(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//将点赞全部设为已读
router.post('/changeStarsType', function (req, res) {
    var par = paramAll(req);

    if (!par.app_sid || !par.openid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.changeStarsType(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//将聊天双方聊天记录设置为已读
router.post('/changeChatType', function (req, res) {
    var par = paramAll(req);

    if (!par.myphone || !par.friendphone || !par.app_sid) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.changeChatType(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//经纬度转化为真实地址
router.post('/locationToAddr', function (req, res) {
    var par = paramAll(req);

    if (!par.location) {
        return res.json(new ERR('参数不全!', 400));
    }

    ArticleDao.locationToAddr(par, function (err, data) {
        if (err) {
            return res.json(new ERR(err, 410));
        }

        return res.json(new PKG(data));
    });
});

//返回公告，type为0返回写作页面公告，type为1返回我的界面公告
router.post('/returnNoticeMessage', function (req, res) {
    var par = paramAll(req);

    if (!par.type) {
        return res.json(new ERR('参数不全!', 400));
    }

    if (par.type == 1) {
        return res.json(new PKG('在消息中可查看所有未读消息!'));
    }

    if (par.type == 0) {
        return res.json(new PKG('写作功能新增图片上传审核和文本审核，检验不合格文本！'));
    }
});

module.exports = router;