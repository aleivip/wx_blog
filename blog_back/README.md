# vue博客

#### 介绍
使用vue + Node + mysql搭建的个人博客

#### 软件架构
软件架构说明


#### 前端安装教程

1. 进入vue_project
2. npm init安装项目依赖
3. npm run dev启动项目
4. npm run build --report打包项目

#### 使用说明

1. 博客整体分为四个功能模块：首页、资源下载、留言板、相册
2. 首页包含前台浏览功能及后台博客管理功能
3. 目前已完成博客基本功能，部分逻辑还需优化
4. 博客发表使用mavon-editor进行文章编辑，可上传图片


#### 后端安装教程

1. 进入blog_back根目录
2. npm init安装项目依赖
3. cd bin && node www启动项目

#### 使用说明

1. 用户信息类接口在users.js路由下
2. 文章类接口在article.js路由下

#项目部分截图
![首页](https://images.gitee.com/uploads/images/2019/0803/175341_b0badfa7_4769407.png "屏幕截图.png")
![后台文章列表](https://images.gitee.com/uploads/images/2019/0803/175409_58400e9b_4769407.png "屏幕截图.png")
![文章发表](https://images.gitee.com/uploads/images/2019/0803/175438_ea452a07_4769407.png "屏幕截图.png")
![文章详情](https://images.gitee.com/uploads/images/2019/0803/175505_40bb65a9_4769407.png "屏幕截图.png")