/**
   * @api {POST} /api/portfolio/portfolios 上传作品
   * @apiDescription 本接口id选传，id未传则查询作品列表，id传值则查询指定作品信息
   * @apiName PostPortfolios
   * @apiGroup portfolios
   * @apiParam {string} albums 图片JSON数组,数组中每一项是一个JSON对象，包含url和type(必传)
   * @apiParam {string} desc 这是发型描述(选传)
   * @apiParam {string} feature 这是特点(选传)
   * @apiParam {string} race 肤色(选传)
   * @apiParam {string} scene 适合场景(选传)
   * @apiParam {string} name 标题(选传)
   * @apiParam {string} popElement 流行元素(选传)
   * @apiParam {string} sceneDesc 适合场景说明(选传)
   * @apiParam {string} effectDesc 效果说明(选传)
   * @apiParam {string} type 图文日记(选传)
   * @apiParam {string} style 风格(选传)
   * @apiParam {string} roll 卷度(选传)
   * @apiParam {string} length 长度(选传)
   * @apiParam {string} price 价格(选传)
   * @apiParam {string} user_id 用户id(选传)
   * @apiParam {string} face_type 脸型(必传)
   * @apiParam {string} hair_quality 发质(选传)
   * @apiParam {string} hair_volume 发量(选传)
   * @apiParam {string} hair_color 发色(选传)
   * @apiParam {string} bangs bangs(选传)
   * @apiParam {string} eye_space 眼间距(选传)
   * @apiParam {string} pop_element 流行元素(选传)
   * @apiParam {string} scene_desc 适合场景说明(选传)
   * @apiParam {string} effect_desc 效果说明(选传)
   * @apiParam {string} three_court 三庭(选传)
   * @apiParam {string} barbershop_id 理发店id(选传)
   * 
   * @apiParamExample {json} 请求示例:
   * {
        "albums": [
          "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
          "http://img01.liwushuo.com/image/150116/yfvethy8a_w.jpg"
        ],
        "face_type": 1
      }
  * @apiSuccessExample {json} 成功响应:
  *     HTTP/1.1 200 OK
  *     {
          "code": 200,
          "portfolio": {
              "id": 27,
              "albums": [
                  {
                      "url": "http://img01.liwushuo.com/image/150116/yfvethy8a_w.jpg",
                      "type": 0
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 1
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 2
                  }
              ],
              "face_type": 1,
              "desc": "xin备注",
              "updatedAt": "2020-01-11T07:54:38.157Z",
              "createdAt": "2020-01-11T07:54:38.157Z"
          }
      }

  *   @apiSuccess {String} albums 上传成功的图片url数组
      @apiSuccess {String} desc 发型描述
      @apiSuccess {String} id 作品id
      @apiSuccess {String} feature 发型特点
      @apiSuccess {String} race 适合肤色
      @apiSuccess {String} scene 适合场景
      @apiSuccess {String} name 标题
      @apiSuccess {String} popElement 流行元素
      @apiSuccess {String} sceneDesc 适合场景说明
      @apiSuccess {String} effectDesc 效果说明
  *  @apiVersion 1.0.0
  */

/**
 * @api {PUT} /api/portfolio/portfolios 更新作品
 * @apiDescription 本接口id选传，id未传则查询作品列表，id传值则查询指定作品信息
 * @apiName Putportfolios
 * @apiGroup portfolios
 * @apiParam {string} id 作品id(必传)
 * @apiParam {string} albums 图片url数组(必传)
   * @apiParam {string} desc 这是发型描述(必传)
   * @apiParam {string} feature 这是特点(必传)
   * @apiParam {string} race 肤色(必传)
   * @apiParam {string} scene 适合场景(必传)
   * @apiParam {string} name 标题(必传)
   * @apiParam {string} popElement 流行元素(必传)
   * @apiParam {string} sceneDesc 适合场景说明(必传)
   * @apiParam {string} effectDesc 效果说明(必传)
   * @apiParam {string} type 图文日记(选传)
   * @apiParam {string} style 风格(选传)
   * @apiParam {string} roll 卷度(选传)
   * @apiParam {string} length 长度(选传)
   * @apiParam {string} price 价格(选传)
   * @apiParam {string} user_id 用户id(选传)
   * @apiParam {string} face_type 脸型(选传)
   * @apiParam {string} hair_quality 发质(选传)
   * @apiParam {string} hair_volume 发量(选传)
   * @apiParam {string} hair_color 发色(选传)
   * @apiParam {string} bangs bangs(选传)
   * @apiParam {string} eye_space 眼间距(选传)
   * @apiParam {string} pop_element 流行元素(选传)
   * @apiParam {string} scene_desc 适合场景说明(选传)
   * @apiParam {string} effect_desc 效果说明(选传)
   * @apiParam {string} three_court 三庭(选传)
   * @apiParam {string} barbershop_id 理发店id(选传)
 * 
 * @apiParamExample {json} 请求示例:
 * {
      id: 1,
      "albums": [
        "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
        "http://img01.liwushuo.com/image/150116/yfvethy8a_w.jpg"
      ],
      "desc": "这是发型描述",
      "feature": "这是特点",
      "race": "肤色",
      "scene": "适合场景",
      "name": "标题",
      "style": 0,
      "roll": 0,
      "length": 0,
      "bangs": 1,
      "price": 0,
      "faceType": 1,
      "hairVolume": 3,
      "eyeSpace": 1,
      "popElement": "流行元素",
      "sceneDesc": "适合场景说明",
      "effectDesc": "效果说明",
      "threeCourt": 1
    }
* @apiSuccessExample {json} 成功响应:
*     HTTP/1.1 200 OK
*     {
          "code": 200,
          "portfolio": {
              "id": 24,
              "type": null,
              "state": 0,
              "albums": [
                  {
                      "url": "http://img01.liwushuo.com/image/150116/yfvethy8a_w.jpg",
                      "type": 0
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 1
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 2
                  }
              ],
              "desc": "xin备注",
              "style": null,
              "roll": null,
              "length": null,
              "feature": null,
              "bangs": null,
              "race": null,
              "scene": null,
              "name": null,
              "price": null,
              "createdAt": "2020-01-11T06:30:51.000Z",
              "updatedAt": "2020-01-11T07:36:12.000Z",
              "user_id": null,
              "like_count": null,
              "share_count": null,
              "face_type": 1,
              "hair_quality": null,
              "hair_volume": null,
              "hair_color": null,
              "eye_space": null,
              "pop_element": null,
              "scene_desc": null,
              "effect_desc": null,
              "three_court": null,
              "barbershop_id": null
          }
      }
*  @apiSuccess {String} albums 上传成功的图片url数组
    @apiSuccess {String} desc 发型描述
    @apiSuccess {String} feature 发型特点
    @apiSuccess {String} race 适合肤色
    @apiSuccess {String} scene 适合场景
    @apiSuccess {String} name 标题
    @apiSuccess {String} popElement 流行元素
    @apiSuccess {String} sceneDesc 适合场景说明
    @apiSuccess {String} effectDesc 效果说明
*  @apiVersion 1.0.0
*/

/**
 * @api {GET} /api/portfolio/portfolios 获取作品
 * @apiDescription 本接口id选传，id未传则查询作品列表，id传值则查询指定作品信息.
 * @apiName portfolios
 * @apiGroup portfolios
 * @apiParam {string} id 作品id(选传)
 * @apiParam {string} page 页码(选传)
 * @apiParam {string} size 每页条数(选传)
 * 
 * @apiParamExample {json} 请求示例:
 *  {
        "id": 1
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *     {
          "code": 200,
          "portfolio": {
              "id": 24,
              "type": null,
              "state": 0,
              "albums": [
                  {
                      "url": "http://img01.liwushuo.com/image/150116/yfvethy8a_w.jpg",
                      "type": 0
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 1
                  },
                  {
                      "url": "http://img03.liwushuo.com/image/160802/9kx839hae.jpg",
                      "type": 2
                  }
              ],
              "desc": "备注",
              "style": null,
              "roll": null,
              "length": null,
              "feature": null,
              "bangs": null,
              "race": null,
              "scene": null,
              "name": null,
              "price": null,
              "createdAt": "2020-01-11T06:30:51.000Z",
              "updatedAt": "2020-01-11T07:16:28.000Z",
              "user_id": null,
              "like_count": null,
              "share_count": null,
              "face_type": 1,
              "hair_quality": null,
              "hair_volume": null,
              "hair_color": null,
              "eye_space": null,
              "pop_element": null,
              "scene_desc": null,
              "effect_desc": null,
              "three_court": null,
              "barbershop_id": null
          }
      }
 *  @apiSuccess {String} albums 上传成功的图片url数组
    @apiSuccess {String} desc 发型描述
    @apiSuccess {String} feature 发型特点
    @apiSuccess {String} race 适合肤色
    @apiSuccess {String} scene 适合场景
    @apiSuccess {String} name 标题
    @apiSuccess {String} popElement 流行元素
    @apiSuccess {String} sceneDesc 适合场景说明
    @apiSuccess {String} effectDesc 效果说明
 *  @apiVersion 1.0.0
 */

/**
 * @api {POST} /api/upload/upload_img 单图上传
 * @apiDescription 本接口用于单图上传七牛云，上传接口必须使用form-data格式提交图片数据
 * @apiName upload
 * @apiGroup portfolios
 * @apiParam {string} file 文件(选传)
 * 
 * @apiParamExample {form-data} 请求示例:
 *  {
        "file": "111.jpg"
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *      {
          "status": 200,
          "payload": {
              "url": "http://pic.mb1949.com/1e0ee5c9-e860-4387-8729-3582ae786311.jpg"
          }
      }
 *  @apiSuccess {String} url 上传成功的图片url
 *  @apiVersion 1.0.0
 */

 /**
 * @api {POST} /api/barbershop/barbershops 创建理发店信息
 * @apiDescription 本接口用于创建理发店信息，目前没有user_id，所以通过uuid随机生成user_id，二期接入会员体系需要优化这部分逻辑
 * @apiName Postbarbershop
 * @apiGroup barbershops
 * @apiParam {string} name 发型师账号(必传)
 * @apiParam {string} tel 发型师电话(必传)
 * @apiParam {string} longitude 理发店所在经度(必传)
 * @apiParam {string} latitude 理发店所在纬度(必传)
 * @apiParam {string} user_id 用户id(选传)
 * @apiParam {string} cellphone 理发店固定电话(选传)
 * 
 * @apiParamExample {json} 请求示例:
 *  {
      "name": "谢志攀",
      "tel": "18859221461",
      "longitude": "118.19679260253906",
      "latitude": "24.48760223388672"
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *      {
          "code": 200,
          "barbershop": {
              "id": 10,
              "name": "周雄伟",
              "province": "福建省",
              "city": "厦门市",
              "street": "思明区",
              "detail": "南投路11号",
              "latitude": "24.48760223388672",
              "longitude": "118.19679260253906",
              "tel": "18506920590",
              "user_id": "4bfdc0f8-9385-4988-9812-c30dc4fe0c35",
              "createdAt": "2020-01-04T07:23:46.000Z",
              "updatedAt": "2020-01-11T08:36:27.364Z"
          }
      }
 *  @apiSuccess {string} name 发型师账号
 *  @apiSuccess {string} tel 发型师电话
 *  @apiSuccess {string} longitude 理发店所在经度
 *  @apiSuccess {string} latitude 理发店所在纬度
 *  @apiSuccess {string} user_id 用户id
 *  @apiSuccess {string} cellphone 理发店固定电话
 *  @apiSuccess {string} province 理发店所在省份
 *  @apiSuccess {string} city 理发店所在城市
 *  @apiSuccess {string} street 理发店所在街道
 *  @apiSuccess {string} detail 理发店所在详细地址
 *  @apiVersion 1.0.0
 */

 /**
 * @api {PUT} /api/barbershop/barbershops 更新理发店信息
 * @apiDescription 本接口用更新理发店信息，通过user_id更新信息，目前没有user_id，所以通过uuid随机生成user_id，二期接入会员体系需要手动维护已生成门店的user_id信息
 * @apiName Putbarbershop
 * @apiGroup barbershops
 * @apiParam {string} name 发型师账号(必传)
 * @apiParam {string} tel 发型师电话(必传)
 * @apiParam {string} longitude 理发店所在经度(必传)
 * @apiParam {string} latitude 理发店所在纬度(必传)
 * @apiParam {string} user_id 用户id(选传)
 * @apiParam {string} cellphone 理发店固定电话(选传)
 * 
 * @apiParamExample {json} 请求示例:
 *  {
      "name": "谢志攀",
      "tel": "18859221461",
      "longitude": "118.19679260253906",
      "latitude": "24.48760223388672"
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *      {
          "code": 200,
          "barbershop": {
              "id": 4,
              "name": "周雄伟",
              "province": "福建省",
              "city": "厦门市",
              "street": "思明区",
              "detail": "南投路11号",
              "latitude": "24.48760223388672",
              "longitude": "118.19679260253906",
              "tel": "18506920590",
              "cellphone": null,
              "user_id": "4bfdc0f8-9385-4988-9812-c30dc4fe0c35",
              "createdAt": "2020-01-04T07:23:46.000Z",
              "updatedAt": "2020-01-11T08:35:07.000Z"
          }
      }
 *  @apiSuccess {string} name 发型师账号
 *  @apiSuccess {string} tel 发型师电话
 *  @apiSuccess {string} longitude 理发店所在经度
 *  @apiSuccess {string} latitude 理发店所在纬度
 *  @apiSuccess {string} user_id 用户id
 *  @apiSuccess {string} cellphone 理发店固定电话
 *  @apiSuccess {string} province 理发店所在省份
 *  @apiSuccess {string} city 理发店所在城市
 *  @apiSuccess {string} street 理发店所在街道
 *  @apiSuccess {string} detail 理发店所在详细地址
 *  @apiVersion 1.0.0
 */

 /**
 * @api {GET} /api/barbershop/barbershops 查询理发店信息
 * @apiDescription 本接口用于查询理发店信息，user_id传值则表示查询指定理发师的门店信息，user_id未传值则查询理发店列表
 * @apiName barbershop
 * @apiGroup barbershops
 * @apiParam {string} user_id 用户id(选传)
 * @apiParam {string} page 页码(选传)
 * @apiParam {string} size 每页数据量(选传)
 * 
 * @apiParamExample {json} 请求示例:
 *  {
      "user_id": "29d116bd-d3a0-4f98-a63b-dae2bb508d8c"
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *      {
          "code": 200,
          "barbershop": {
              "id": 4,
              "name": "周雄伟",
              "province": "福建省",
              "city": "厦门市",
              "street": "思明区",
              "detail": "宜兰路7号",
              "latitude": "24.48760223388672",
              "longitude": "118.19679260253906",
              "tel": "18506920590",
              "user_id": "4bfdc0f8-9385-4988-9812-c30dc4fe0c35",
              "createdAt": "2020-01-04T07:23:46.000Z",
              "updatedAt": "2020-01-04T07:23:46.000Z"
          }
      }
 *  @apiSuccess {string} name 发型师账号
 *  @apiSuccess {string} tel 发型师电话
 *  @apiSuccess {string} longitude 理发店所在经度
 *  @apiSuccess {string} latitude 理发店所在纬度
 *  @apiSuccess {string} user_id 用户id
 *  @apiSuccess {string} province 理发店所在省份
 *  @apiSuccess {string} city 理发店所在城市
 *  @apiSuccess {string} street 理发店所在街道
 *  @apiSuccess {string} detail 理发店所在详细地址
 *  @apiVersion 1.0.0
 */




 /**
 * @api {GET} /api/users/regist 用户授权登录
 * @apiDescription 前端获取code，通过code和app_sid授权登录，如果openid在用户表已存在，则直接生成token登录，如果openid不存在，则获取用户信息进行注册并生成token返回
 * @apiName regist
 * @apiGroup users
 * @apiParam {string} code 授权登录得到的code(必传)
 * @apiParam {string} app_sid 用户类别，user表示正常用户，barbershop表示理发师，admin表示管理员(必传)
 * 
 * @apiParamExample {json} 请求示例:
 *  {
      "code": "29d116bd-d3a0-4f98-a63b-dae2bb508d8c",
      "app_sid": "user"
    }
 * @apiSuccessExample {json} 成功响应:
 *     HTTP/1.1 200 OK
 *      {
          "code": 200,
          "payload": {
              "id": 4,
              "uuid": "29d116bd-d3a0-4f98-a63b-dae2bb508d8c",
              "wx_nick": "周周周周周",
              "wx_avatar": "http://www.baidu.com",
              "wx_openid": "29d116bd-d3a0-4f98-a63b-dae2bb508d8c",
              "app_sid": "user",
              "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
          }
      }
 *  @apiSuccess {string} uuid 用户唯一标识
 *  @apiSuccess {string} wx_nick 用户昵称，默认使用微信名，后期可以开放修改
 *  @apiSuccess {string} wx_avatar 用户头像，默认使用微信头像，后期可以开放修改
 *  @apiSuccess {string} wx_openid 用户openid
 *  @apiSuccess {string} app_sid 用户类别
 *  @apiSuccess {string} token 鉴权参数
 *  @apiVersion 1.0.0
 */